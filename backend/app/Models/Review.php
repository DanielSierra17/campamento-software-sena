<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['title','text','rating', 'user_id', 'bootcamp_id'];
    use HasFactory;
}