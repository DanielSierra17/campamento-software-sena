<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            "title"=>$this->title,
            "text"=>$this->text,
            "rating"=>$this->rating,
            "user_id"=>$this->user_id,
            "bootcamp_id"=>$this->bootcamp_id
        ];
    }
}
