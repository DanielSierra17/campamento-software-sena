<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Review;
use App\Http\Requests\StoreReviewRequest;
use App\Http\Resources\ReviewCollection;
use App\Http\Resources\ReviewResource;
use App\Http\Controllers\BaseController;

class ReviewController extends BaseController
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //metodo json
        //parametros: 1. data a encier al cliente
        //            2. codigo status http
        // return response()->json( new Bootcampcollection(Bootcamp::all())
        //                             ,200);
        try{
            return $this->sendResponse(new ReviewCollection(Course::all()));
        }catch(\Exception $e){
            return $this->sendError( 'server error',500 );
        }
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $review = new Review();
        $review->bootcamp_id = $id;
        $review->title = $request->title;
        $review->text = $request->text;
        $review->rating = $request->rating;
        $review->user_id = $request->user_id;
        $review->save();

        return response()->json( [
                                    "success" => true,
                                    "data" => $review
                                ] , 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json( [ "success" => true,
                                    "data" => new ReviewResource(Review::find($id))
                                     ] ,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $b = Review::find($id);
        //actializar con update
        $b->update($request->all());
        return response()->json([ "success" => true,
                                    "data" => new ReviewResource($b)
                                ] , 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $b = Review::find($id);
        $b->delete($id);
        return $this->sendResponse(new ReviewResource($b));
    }
}
