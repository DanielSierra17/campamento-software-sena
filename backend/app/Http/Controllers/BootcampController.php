<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Bootcamp;
use App\Http\Requests\StoreBootcampRequest;
use App\Http\Resources\BootcampCollection;
use App\Http\Resources\BootcampResource;
use App\Http\Controllers\BaseController;

class BootcampController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            return $this->sendResponse( new BootcampCollection(Bootcamp::all()));

        }catch(\Exception $e) {

            return $this->sendError('Servidor fuera de linea, por favor intentelo mas tarde', 500);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBootcampRequest $request)
    {
        $c = new Bootcamp();

        $c->name = request->name;
        $c->description = request->description;
        $c->website = request->website;
        $c->phone = request->phone;
        $c-> user_id = request->user_id;
        $c-> average_rating = request->average_rating;
        $c-> average_cost = request->average_cost;
        $c->save();
        try {
        return $this->sendResponse(new BootcampResource($c), 201);
        } catch (\Exception $e) {
            return $this->sendError('Servidor fuera de linea, por favor intentelo mas tarde', 500);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $bootcamp = Bootcamp::find($id);

        if(!$bootcamp) {

            return $this->sendError("Bootcamp con id: $id no existe", 400);

        }

        return $this->sendResponse( new BootcampResource($bootcamp));
        } catch (\Exception $e) {
            return $this->sendError('Servidor fuera de linea, por favor intentelo mas tarde', 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $b = Bootcamp::find($id);

        if(!$b) {

            return $this->sendError("Bootcamp con id: $id no existe", 400);

        }

        $b->update($request->all());

        return $this->sendResponse( new BootcampResource($b));
        } catch (\Exception $e) {
            //throw $th;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try {
        $a= Bootcamp::find($id);

        if(!$a) {

            return $this->sendError("Bootcamp con id: $id no existe", 400);

        }

        $a->delete($id);

        return $this->sendResponse( new BootcampResource($a));
       } catch (\Exception $e) {
            return $this->sendError('Servidor fuera de linea, por favor intentelo mas tarde', 500);
       } 
    }
}